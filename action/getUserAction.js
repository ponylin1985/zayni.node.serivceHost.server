'use strict';

const common           = require( 'zayni' ).common;
const entity           = require( 'zayni/entity' );
const serivceHost      = require( 'zayni/serviceHost' );
const ServiceAction    = serivceHost.ServiceAction;
const ServiceContainer = serivceHost.ServiceActionContainer;
const Result           = entity.Result;

class GetUserAction extends ServiceAction {
    constructor() {
        super( 'Test' );
    }

    execute() {
        let request = this.requestData;
        console.log( `UserId id: ${request.userId}.` );
        console.log( `UserName: ${request.userName}` );
        console.log( `Sex: ${request.sex}` );
        console.log( `Age: ${request.age}` );

        let model = {
            userId     : request.userId,
            userSecret : common.createRandomText( 5 ) + '-' + common.createRandomText( 5 ) + '-' + common.createRandomText( 5 ),
            accountNo  : `U228713987`,
            birthday   : new Date( '1985-10-24' ),
            girlFriend : [
                {
                    name         : 'Amber',
                    special      : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck', 'Drink Sperm' ],
                    age          : 21,
                    isSlut       : true,
                    isMyFavorite : true
                },
                {
                    name    : 'Cindy',
                    special : [ 'Angl Fuck', 'Deep Thoat Fuck', '3P Fuck' ],
                    age     : 24,
                    isBitch : true
                }
            ]
        };

        let result       = new Result( true );
        result.data      = model;
        result.code      = 200;
        result.message   = 'OK';
        result.isSuccess = true;

        super.result = result;
    }
}

let r = ServiceContainer.addServiceAction( new GetUserAction() );

if ( !r.isSuccess ) {
    console.error( r.message );
    throw new Error( r.message );
    return;
}

module.exports = GetUserAction;