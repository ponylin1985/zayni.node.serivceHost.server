'use strict';

const ServiceHost = require( 'zayni/serviceHost' ).ServiceHost;
const action      = require( './action/getUserAction.js' );
const logger      = require( 'zayni/log' );

logger.useConfig( {
    enableConsoleLog : true,
    enableFileLog    : true,
    fileLogPath      : __dirname + '/ServerLog/zayni.logger.test.log',
    logFileMaxSize   : 2
} );

let host = new ServiceHost();
host.startHost( '127.0.0.1', 7765 );

console.log( 'Waiting for RPC request...' );